BRUTALDOOM.ENTITIES =
{   
    --Urban Junk
    DestroyedCarGreen = { id=11004, r=28, h=48 }
    DestroyedCarBlue = { id=11005, r=28, h=48 }
    DestroyedCarRed = { id=11006, r=28, h=48 }
    DestroyedCarGray = { id=11007, r=28, h=48 }
    BDECburningtires = { id=11010, r=24, h=12 }
    DestroyedLamppost = { id=11011, r=8, h=64 }
    UrbanRubble = { id=11001, r=14, h=4 }
    BusSign = { id=11002, r=12, h=4 }
    StopSign = { id=11003, r=12, h=4 }
    
    BDCritterMouse = { id=367, r=6, h=6 }
    
    BDECGrass = { id=15848, r=12, h=4 }
    BDECBush = { id=15849, r=12, h=4 }
    BDECGreenTree1 = { id=15850, r=12, h=4 }
    BDECGreenTree2 = { id=15851, r=12, h=4 }
    BDECGreenTree3 = { id=15852, r=12, h=4 }
    BDECBarrel = { id=15855, r=12, h=40 }
    BDECChainHook = { id=15856, r=2, h=80, ceil=true }
    
    BDECCagedLight = { id=15857, r=12, h=4, ceil=true }
    BDECIndustrialLamp = { id=15878, r=8, h=4, ceil=true }
    BDECCagedLight2 = { id=15859, r=12, h=20, ceil=true }
    
    BDECUACShip = { id=13845, r=321, h=200 }

    LargeRainSpawner = { id=15868, r=1, h=1 }
    SkyboxRainSpawner = { id=373, r=1, h=1 }

}
