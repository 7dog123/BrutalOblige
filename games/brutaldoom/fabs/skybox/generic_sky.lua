PREFABS.Skybox_generic =
{
  file  = "skybox/generic_sky.wad"
  map   = "MAP01"

  prob  = 50

  kind  = "skybox"

  bound_z0 = -256
  bound_z1 = 256
}
