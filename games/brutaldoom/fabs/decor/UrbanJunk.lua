PREFABS.Decor_DestroyedCar =
{
  file   = "decor/UrbanJunk.wad"
  map    = "MAP01"
  
  kind = "decor"

  prob   = 9000
  theme  = "urban"
  env    = "outdoor"
  
  size = 128

  where  = "point"
  
  thing_11004 = 
  {
    DestroyedCarBlue = 50
    DestroyedCarRed = 50
    DestroyedCarGray = 50
  }
}

